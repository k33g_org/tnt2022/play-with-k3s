#!/bin/bash
set -o allexport; source .env; set +o allexport

civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
civo kubernetes remove ${CLUSTER_NAME} --region=${CLUSTER_REGION} --yes 

rm ./config/k3s.yaml
rm ./config/certificate.txt
rm ./config/url.txt
rm ./config/external_ip.txt
rm ./config/dns.txt
rm ./config/infos.txt
